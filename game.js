var platforms = [];
var lastTime = 0;
var mainState = {
    preload: function() {

    },
    create: function() {
        
        game.add.image(0, 0, 'background'); 
        
        this.cursor = game.input.keyboard.createCursorKeys();
        
        this.player = game.add.sprite(200, 50, 'player');
        this.player.facingLeft = false;


        /// ToDo: Add 4 animations.
        /// 1. Create the 'rightwalk' animation by looping the frames 1 and 2
        this.player.animations.add('rightwalk', [1,2], 8, true);
        /// 2. Create the 'leftwalk' animation by looping the frames 3 and 4
        this.player.animations.add('leftwalk', [3,4], 8, true);
        /// 3. Create the 'rightjump' animation (frames 5 and 6 and no loop)
        this.player.animations.add('rightjump', [5,6], 8, false);
        /// 4. Create the 'leftjump' animation (frames 7 and 8 and no loop)
        this.player.animations.add('leftjump', [7,8], 8, false);
        ///

        /// Add wall
        this.rightwall = game.add.sprite(620, 0, 'wall'); 
        game.physics.arcade.enable(this.rightwall);
        this.rightwall.body.immovable = true;

        this.leftwall = game.add.sprite(0, 0, 'wall'); 
        game.physics.arcade.enable(this.leftwall);
        this.leftwall.body.immovable = true;

        this.ceiling = game.add.sprite(30, 0, 'ceiling'); 
        game.physics.arcade.enable(this.ceiling);
        this.ceiling.body.immovable = true;


        this.scoreLabel = game.add.text(40, 450, 'score: 0', { font: '25px Arial', fill: '#ffffff' });
        game.global.score = 0;
        this.lifeLabel = game.add.text(550, 450, 'life: 5', { font: '25px Arial', fill: '#ffffff' });
        this.player.life = 5;
        game.physics.arcade.enable(this.player);

        // Add vertical gravity to the player

        this.player.body.gravity.y = 500;
        this.player.touchOn = undefined;

        this.dieSound = game.add.audio('die');
        this.backSound = game.add.audio('back');
        this.backSound.play();

    },
   
  
    update: function() {
        
        game.physics.arcade.collide(this.player, platforms, this.effect);
        game.physics.arcade.collide(this.player, [this.leftwall, this.rightwall]);
        
        

        if(this.player.body.y < 0) {
            if(this.player.body.velocity.y < 0) {
                this.player.body.velocity.y = 0;
            }
            //if(game.time.now > this.player.unbeatableTime) {
                
                this.playerDie();
                game.camera.flash(0xff0000, 100);
                //this.player.unbeatableTime = game.time.now + 2000;
            //}
        }
        if(this.player.life==0){
            this.playerDie();
        }

        this.updatePlatforms();
        this.createPlatforms();
        
        if (!this.player.inWorld) { this.playerDie();}
        this.movePlayer();

       
    }, 

    
    createPlatforms: function () {
        if(game.time.now > lastTime + 600) {
            lastTime = game.time.now;
            this.createOnePlatform();
            game.global.score+=1;
            this.scoreLabel.text = 'score:'+game.global.score;
        }
    },

    createOnePlatform: function() {

        var platform;
        var x = Math.random()*(650 - 100 - 40) + 20;
        var y = 500;
        var rand = Math.random() * 100;

        if(rand < 50) {
            platform = game.add.sprite(x, y, 'ground');
        } else  {
            platform = game.add.sprite(x, y, 'sharp');
            game.physics.arcade.enable(platform);
            platform.body.setSize(96, 15, 0, 0);
        }

        game.physics.arcade.enable(platform);
        platform.body.immovable = true;
        platforms.push(platform);

        platform.body.checkCollision.down = false;
        platform.body.checkCollision.left = false;
        platform.body.checkCollision.right = false;
    },

    updatePlatforms: function(){
        for(var i=0; i<platforms.length; i++) {
            //var platform = platforms[i];
            platforms[i].body.position.y -= 2;
            if(platforms[i].body.position.y <= -20) {
                platforms[i].destroy();
                platforms.splice(i, 1);
            }
        }
    },
    
     effect: function(platform) {
        if(platform.key == 'sharp') {
            
            if (this.player.touchOn !== platform) {
                this.player.touchOn = platform;
                this.player.life -=1;
                this.lifeLabel.text = 'life:'+this.player.life;
                game.camera.flash(0xff0000, 100);
            }
        }
        if(platform.key == 'ground') {
            if (this.player.touchOn !== platform) {
                this.player.touchOn = platform;
            }
        }
    },
    

    playerDie: function() { 
        this.dieSound.play();
        this.backSound.stop();
        game.state.start('gameover');
    },

    /// ToDo: Finish the 4 animation part.
    movePlayer: function() {
        if (this.cursor.left.isDown) {
            this.player.body.velocity.x = -200;
            this.player.facingLeft = true;

            /// 1. Play the animation 'leftwalk'
            this.player.animations.play('leftwalk');
            ///
        }
        else if (this.cursor.right.isDown) { 
            this.player.body.velocity.x = 200;
            this.player.facingLeft = false;

            /// 2. Play the animation 'rightwalk' 
            this.player.animations.play('rightwalk');
            ///
        }    

        // If the up arrow key is pressed, And the player is on the ground.
        else if (this.cursor.up.isDown) { 
            if(this.player.body.touching.down){
                // Move the player upward (jump)
                if(this.player.facingLeft) {
                    /// 3. Play the 'leftjump' animation
                    this.player.animations.play('leftjump');
                    ///
                }else {
                    /// 4. Play the 'rightjump' animation
                    this.player.animations.play('rightjump');
                    ///
                }
                this.player.body.velocity.y = -350;
            }
        }  
        // If neither the right or left arrow key is pressed
        else {
            // Stop the player 
            this.player.body.velocity.x = 0;
        
            if(this.player.facingLeft) {
                // Change player frame to 3 (Facing left)
                this.player.frame = 3;
            }else {
                // Change player frame to 1 (Facing right)
                this.player.frame = 1;
            }

            // Stop the animation
            this.player.animations.stop();
        }    
    }
    
};
   



