var menuState={

    create:function(){
        game.add.image(0, 0, 'background');

        var nameLabel = game.add.text(game.width/2, 150, 'Leap of Faith', {font: '50px Arial', fill:'#ffffff'});
        nameLabel.anchor.setTo(0.5, 0.5);

        var scoreLabel = game.add.text(game.width/2, 300, 'Score: '+game.global.score, {font: '35px Arial', fill:'#ffffff'});
        scoreLabel.anchor.setTo(0.5, 0.5);

        var startLabel = game.add.text(game.width/2, game.height-80, 'press the up arrow key to start', {font: '25px Arial', fill:'#ffffff'});
        startLabel.anchor.setTo(0.5, 0.5);

        this.menuSound = game.add.audio('menuMusic');
        this.menuSound.play();

        var upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
        upKey.onDown.add(this.start, this);
    },
    start: function(){
        game.state.start('main');
        this.menuSound.stop();
    }
};