var game = new Phaser.Game(650, 500, Phaser.AUTO, 'canvas');

game.global={score:0};

game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('main', mainState);
game.state.add('gameover', gameoverState);

game.state.start('boot');