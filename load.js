var loadState={

    preload:function(){
        var loadingLabel = game.add.text(game.width/2, 150, 'loading...', {font: '30px Arial', fill:'#ffffff'});
        loadingLabel.anchor.setTo(0.5, 0.5);

        var progressBar = game.add.sprite(game.width/2, 200, 'progressBar');
        progressBar.anchor.setTo(0.5, 0.5);
        game.load.setPreloadSprite(progressBar);

        game.load.image('background', 'assets/background.png');
        game.load.image('ground', 'assets/ground.png');
        game.load.image('ceiling', 'assets/ceiling.png');
        game.load.image('wall', 'assets/wall.jpg');
        game.load.image('sharp', 'assets/sharp.png');
        game.load.image('stone', 'assets/stone.jpg');
 
        game.load.spritesheet('player', 'assets/MARIO.png', 32, 54);

        game.load.image('background', 'assets/background.png');

        game.load.audio('die','assets/die.mp3');
        game.load.audio('back','assets/back.mp3');
        game.load.audio('menuMusic','assets/menuMusic.mp3');
    },

    create:function(){
        game.state.start('menu');
    }
};